import * as fs from 'fs';
import * as dotenv from 'dotenv';

interface DatabaseInfo {
  name: string;
  user: string;
  password: string;
  host: string;
  port: number;
}

interface EnvConfig {
  PORT: number;
  bodyLimit: string;
  jwtSecret: string;
  slackToken: string;
  slackChannelId: string;
  database: DatabaseInfo;
}

class ConfigServiceClass {
  public readonly env: EnvConfig;

  constructor(filePath?: string) {
    // console.log('filepath', filePath);
    const config = dotenv.parse(fs.readFileSync(filePath ? filePath : '.env'));
    // console.log('config', config);
    // this.envConfig = this.validateInput(config);
    this.env = {
      PORT: config.PORT,
      bodyLimit: config.bodyLimit,
      jwtSecret: config.jwtSecret,
      slackChannelId: config.slackChannelId,
      slackToken: config.slackToken,
      database: {
        user: config.dbUser,
        password: config.dbPassword,
        name: config.dbName,
        host: config.dbHost || 'localhost',
        port: config.dbPort || 3306,
      },
    };
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    return null;
  }
}

export const ConfigSerivce = new ConfigServiceClass();
