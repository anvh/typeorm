import { ExpressMiddlewareInterface } from 'routing-controllers';
import { CacheService } from '../common/cache.service';

export class CacheMiddleware implements ExpressMiddlewareInterface {
  // interface implementation is optional
  // TODO: add token into cache key
  async use(
    request: any,
    response: any,
    next?: (err?: any) => any,
  ): Promise<any> {
    try {
      const key = request.originalUrl || request.url;
      const content = await CacheService.get(key);
      if (content) {
        console.log('from cache');
        response.send(content);
      } else {
        next();
      }
    } catch (error) {
      next();
    }
  }
}
