import { Middleware, ExpressMiddlewareInterface } from 'routing-controllers';
import * as jwt from 'jsonwebtoken';
import { ConfigSerivce } from '../config/config.service';

@Middleware({ type: 'before' })
export class AuthenMiddleware implements ExpressMiddlewareInterface {
  async use(request: any, response: any, next: any): Promise<any> {
    const secretKey = ConfigSerivce.env.jwtSecret || 'UcC-:{J7J8gv&VP(';
    const tokenInput = request.headers.authorization;
    try {
      let token: string;
      if (tokenInput[0] === 'B') {
        token = tokenInput.split('Bearer ')[1];
      } else {
        token = tokenInput.split('bearer ')[1];
      }
      const payload: any = await jwt.verify(token, secretKey);
      request.user = payload.data;
      next();
    } catch (error) {
      next();
    }
  }
}
