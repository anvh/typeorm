import { WebClient } from '@slack/client';
import { ConfigSerivce } from '../config/config.service';
class Logger {
  private readonly slackClient: WebClient;
  constructor() {
    if (ConfigSerivce.env.slackToken) {
      this.slackClient = new WebClient(ConfigSerivce.env.slackToken);
    }
  }
  public info(...message) {
    console.info(message);
  }
  public log(...message) {
    console.log(message);
  }
  public error(...message) {
    console.error(message);
  }
  public async slack(message) {
    if (this.slackClient) {
      let text: string;
      if (typeof message === 'object') {
        text = JSON.stringify(message, null, 2);
      } else {
        text = message;
      }
      text = '```' + text + '```';
      await this.slackClient.chat.postMessage({
        channel: ConfigSerivce.env.slackChannelId,
        text,
      });
    }
  }
}

export const logger = new Logger();
