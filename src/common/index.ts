import { TestController } from '../modules/test/test.controller';

export type Lazy<T extends object> = Promise<T> | T;

const createController = () => {
  const arrayController = [];
  arrayController.push(TestController);
  return arrayController;
};

export const expressListController = createController();
