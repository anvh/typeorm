import 'reflect-metadata'; // this shim is required
import { createConnections } from 'typeorm';
import {
  createExpressServer,
  Action,
} from 'routing-controllers';
import { AuthenMiddleware } from './middlewares/auth.middleware';
import { LogMiddleware } from './middlewares/log.middleware';
import { Express } from 'express';
import { logger } from './common/log.service';
import { ConfigSerivce } from './config/config.service';
import { expressListController } from './common';

const routingControllersOptions = {
  authorizationChecker: async (action: Action, roles: string[]) => {
    const user = action.request.user;
    if (user && !roles.length) {
      return true;
    }
    if (user && roles.find(role => user.roles.indexOf(role) !== -1)) {
      return true;
    }
    return false;
  },
  currentUserChecker: (action: Action) => {
    return action.request.user;
  },
  controllers: expressListController, // we specify controllers we want to use
  middlewares: [AuthenMiddleware, LogMiddleware],
  // routePrefix: '/v1'
};

// creates express app, registers all controller routes and returns you express app instance
const app: Express = createExpressServer(routingControllersOptions);

// run express application on port 3009
app.listen(3009, err => {
  if (err) {
    logger.error('server.cannot.start');
  }
  logger.info('server.started', { time: new Date() });
});

const bootstrap = async () => {
  const dbConnect = [];
  dbConnect.push({
    name: 'dbconnection',
    type: 'postgres',
    host: ConfigSerivce.env.database.host,
    port: ConfigSerivce.env.database.port,
    username: ConfigSerivce.env.database.user,
    password: ConfigSerivce.env.database.password,
    database: ConfigSerivce.env.database.name,
    entities: [__dirname + '/entity/test/*{.js,.ts}'],
    synchronize: true,
  });
  await createConnections(dbConnect);
};
bootstrap();
