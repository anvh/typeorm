import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import Building from './Building';
import User from './User';

@Entity()
export default class Barrage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  text: string;

  @ManyToOne((type: any) => Building, (building: Building) => building.barrages)
  building: Building;

  @ManyToOne((type: any) => User, (user: User) => user.postBarrages)
  postUser: User;

  // @ManyToMany((type: any) => User, (user: User) => user.likedBarrages)
  // likedUsers: User[];

}
