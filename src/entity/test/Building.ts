import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import Barrage from './Barrage';

@Entity()
export default class Building {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @Column()
  introductionText: string;

  @Column()
  imageUrl: string;

  @OneToMany((type: any) => Barrage, (barrage: Barrage) => barrage.building)
  barrages: Barrage[];
}
