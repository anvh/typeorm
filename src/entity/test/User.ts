import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import Barrage from './Barrage';

@Entity()
export default class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  openid: string;

  @OneToMany((type: any) => Barrage, (barrage: Barrage) => barrage.postUser)
  postBarrages: Barrage[];

  // @ManyToMany((type: any) => Barrage, (barrage: Barrage) => barrage.likedUsers)
  // @JoinTable()
  // likedBarrages: Barrage[];
}
