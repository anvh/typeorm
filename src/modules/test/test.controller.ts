import {
  Controller,
  Get,
  Post,
  Patch,
} from 'routing-controllers';

@Controller('/test')
export class TestController {
  @Post()
  async createUser() { }
}
